DROP TABLE IF EXISTS incidente;
DROP TABLE IF EXISTS usuario;


create table usuario(
    id_usuario serial,
    correo varchar(100),
    clave varchar(2000),
    roles json,
    constraint pk_usuario primary key (id_usuario)
);

create table incidente(
    id_incidente serial,
    tipo varchar(100),
    descripcion varchar(1000),
	estado varchar(1),
	costo real,
	observacion varchar(1000),
    roles json,
	id_usuario int,
	
	constraint pk_incidente primary key (id_incidente),
	constraint fk_incidente_usuario foreign key (id_usuario)
        references usuario (id_usuario)
);